# ANTsPyNet

This is a simple Dockerfile containing [ANTsPyNet](https://github.com/ANTsX/ANTsPyNet) and its dependencies.

It uses Python 3.11 because pip cannot find the right dependencies for a newer Python.

The [recommended Dockerfile](https://hub.docker.com/r/cookpa/antspynet) is difficult to execute in our pipelines, especially with Docker and mounted directories.

